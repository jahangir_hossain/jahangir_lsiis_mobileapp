package com.example.edoc;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SignUpActivity extends AppCompatActivity {

    TextView usetTypeErrMsg;
    EditText userField, passwordField;
    LinearLayout type_student, type_intern, type_doctor, type_hospital;
    Button nextButton;
    boolean isVisible;
    int userType;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        isVisible = true;
        userType = -1;

        super.onCreate(savedInstanceState);
        //remove the action bar & make the app full screen
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide(); // hide the title bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN); //enable full screen


        setContentView(R.layout.activity_sign_up);

        usetTypeErrMsg = (TextView) findViewById(R.id.user_type_err_msg);

        userField = (EditText)findViewById(R.id.user_field_id);
        passwordField = (EditText)findViewById(R.id.password_field_id);

        type_student = (LinearLayout) findViewById(R.id.type_student);
        type_intern = (LinearLayout) findViewById(R.id.type_intern);
        type_doctor = (LinearLayout) findViewById(R.id.type_doctor);
        type_hospital = (LinearLayout) findViewById(R.id.type_hospital);

        nextButton = (Button) findViewById(R.id.next_btn);
        nextButton.setEnabled(false);

        userField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                activateNextButton();

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        passwordField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                activateNextButton();

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        passwordField.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
//                final int DRAWABLE_LEFT = 0;
//                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
//                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (passwordField.getRight() - passwordField.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here
                        isVisible=!isVisible;
                        if (isVisible)
                        {
                            // Show Password
                            passwordField.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                        }
                        else
                        {
                            // Hide Password
                            passwordField.setTransformationMethod(PasswordTransformationMethod.getInstance());
                        }

                        return true;
                    }
                }
                return false;
            }
        });



        type_student.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userType = 0;
                setDefaultBackgroundColor();
                activateNextButton();
                type_student.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
            }
        });

        type_intern.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userType = 1;
                setDefaultBackgroundColor();
                activateNextButton();
                type_intern.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

            }
        });

        type_doctor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userType = 2;
                setDefaultBackgroundColor();
                activateNextButton();
                type_doctor.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

            }
        });

        type_hospital.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                userType = 3;
                setDefaultBackgroundColor();
                activateNextButton();
                type_hospital.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

            }
        });


        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateUser()){
                    Intent intent = new Intent(SignUpActivity.this, SignUpEmailVerificationActivity.class);
                    startActivity(intent);
                }
            }
        });




    }

    void setDefaultBackgroundColor(){

        type_student.setBackgroundColor(getResources().getColor(R.color.lightBlue));
        type_intern.setBackgroundColor(getResources().getColor(R.color.lightBlue));
        type_doctor.setBackgroundColor(getResources().getColor(R.color.lightBlue));
        type_hospital.setBackgroundColor(getResources().getColor(R.color.lightBlue));


    }


    boolean validateUser() {
        boolean isValidUser = false, isValidPassword = false, isValidUserType = false;

        if (userField.getText().toString().isEmpty()) {
            userField.setError("UserID is required");
            isValidUser = false;

        }else if(!Patterns.EMAIL_ADDRESS.matcher(userField.getText().toString()).matches()){
            userField.setError("Enter a valid email");
            isValidUser = false;

        }else{


            //hit server
            isValidUser = true;

        }

        if(passwordField.getText().toString().isEmpty()){
            passwordField.setError("Password is required");
            isValidPassword = false;
        }else if(passwordField.getText().toString().length() < 8){
            passwordField.setError("password must be 8 characters or more");
            isValidPassword = false;
        }else {

            //hit server
            isValidPassword = true;

        }

        if(0 <= userType && userType <= 3){
            isValidUserType =true;
            usetTypeErrMsg.setText("");

        }else {
            usetTypeErrMsg.setText("Please select user type");
        }

        return isValidUser && isValidPassword && isValidUserType;

    }

    boolean hasFeildsData(){

        boolean hasUserInput = false, hasPasswordInput = false, hasUserTypeInput = false;
        if(!userField.getText().toString().isEmpty()){
            hasUserInput =true;
        }

        if(!passwordField.getText().toString().isEmpty()){
            hasPasswordInput = true;
        }

        if(0 <= userType && userType <= 3){
            hasUserTypeInput = true;
        }

        return hasUserInput && hasPasswordInput && hasUserTypeInput;

    }

    void activateNextButton(){

        if(hasFeildsData()){
            nextButton.setBackgroundColor(getResources().getColor(R.color.blue));
            nextButton.setEnabled(true);
        }else{
            nextButton.setBackgroundColor(getResources().getColor(R.color.grey));
            nextButton.setEnabled(false);
        }


    }
}
