package com.example.edoc;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.nfc.Tag;
import android.os.Process;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

public class SignUpBasicInfoActivity extends AppCompatActivity {

    String firstName ="", lastName ="", firstNameKana = "", lastNamekana = "", dateOfBirth = "", gender = "";
    int progress = 0;

    private static final String TAG = "SignUpBasicInfoActivity";


    private TextView mDisplayDate;
    private EditText firstNameEt, lastNameEt, firstNameKanaEt, lastNameKanaEt;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    private  Spinner dateSelector;
    private ProgressBar progressBar;

    private RadioGroup radioSexGroup;
    private RadioButton radioSexButton;
    private Button nextButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //remove the action bar & make the app full screen
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide(); // hide the title bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN); //enable full screen

        setContentView(R.layout.activity_sign_up_basic_info);


        mDisplayDate = (TextView) findViewById(R.id.selected_date);
        dateSelector = (Spinner) findViewById(R.id.date);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        progressBar.setProgress(0);
        progressBar.setMax(100);

        firstNameEt = (EditText) findViewById(R.id.first_name);
        lastNameEt = (EditText) findViewById(R.id.last_name);
        firstNameKanaEt = (EditText) findViewById(R.id.furigana_first_name);
        lastNameKanaEt = (EditText) findViewById(R.id.furigana_last_name);
        nextButton = (Button) findViewById(R.id.next_btn);
        nextButton.setEnabled(false);


        addListenerOnButton();
        addListenerOnEditText();


        nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(hasFeildsData()){
                    Intent intent = new Intent(SignUpBasicInfoActivity.this, SignUpBasicInfo2Activity.class);
                    startActivity(intent);
                }
            }
        });

        mDisplayDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        SignUpBasicInfoActivity.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                Log.d(TAG, "onDateSet: mm/dd/yyy: " + month + "/" + day + "/" + year);

                 dateOfBirth = month + "/" + day + "/" + year;
                mDisplayDate.setText(dateOfBirth);
                progress++;
                activateNextButton();
            }
        };



    }

    public void addListenerOnButton() {

        radioSexGroup = (RadioGroup) findViewById(R.id.radioSex);

        radioSexGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // get selected radio button from radioGroup
                int selectedId = radioSexGroup.getCheckedRadioButtonId();

                // find the radiobutton by returned id
                radioSexButton = (RadioButton) findViewById(selectedId);
                gender = radioSexButton.getText().toString();
                progress++;
                activateNextButton();
                Toast.makeText(SignUpBasicInfoActivity.this,
                        radioSexButton.getText(), Toast.LENGTH_SHORT).show();

            }
        });



    }

    public void addListenerOnEditText(){
        firstNameEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() ==  1){
                    progress++;

                }
                activateNextButton();

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        lastNameEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() ==  1){
                    progress++;

                }
                activateNextButton();

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        firstNameKanaEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() ==  0){
                    progress++;

                }

                activateNextButton();

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        lastNameKanaEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length() ==  1){
                    progress++;

                }
                activateNextButton();

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    boolean hasFeildsData(){

        boolean hasFNameInput = false, hasLNameInput = false, hasFNameKanaInput = false, hasLNameKanaInput = false, hasDateOfBirthInput = false, hasGender= false;

        if(!firstNameEt.getText().toString().isEmpty()){
            hasFNameInput =true;
        }

        if(!lastNameEt.getText().toString().isEmpty()){
            hasLNameInput =true;
        }
        if(!firstNameKanaEt.getText().toString().isEmpty()){
            hasFNameKanaInput =true;
        }
        if(!lastNameKanaEt.getText().toString().isEmpty()){
            hasLNameKanaInput =true;
        }
        if(!dateOfBirth.isEmpty()){
            hasDateOfBirthInput =true;
        }
        if(!gender.isEmpty()){
            hasGender =true;
        }



        return hasFNameInput && hasLNameInput && hasFNameKanaInput && hasLNameKanaInput && hasDateOfBirthInput && hasGender;

    }

    void activateNextButton(){
        progressBar.setProgress(progress * 20);

        if(hasFeildsData()){
            nextButton.setBackgroundColor(getResources().getColor(R.color.blue));
            nextButton.setEnabled(true);
        }else{
            nextButton.setBackgroundColor(getResources().getColor(R.color.grey));
            nextButton.setEnabled(false);
        }


    }
}
