package com.example.edoc;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Patterns;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.regex.Pattern;

public class LoginActivity extends AppCompatActivity {

    String userErr, passwordErr;

    private EditText userField, passwordField;
    boolean isVisible;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        isVisible = true;
        super.onCreate(savedInstanceState);

        //remove the action bar & make the app full screen
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide(); // hide the title bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN); //enable full screen

        setContentView(R.layout.activity_login);

        //navigate to sign up activity
        TextView sign_up = (TextView) findViewById(R.id.sign_up);
        sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(intent);
            }
        });


        Button login_btn = (Button) findViewById(R.id.login_btn);
        login_btn.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public void onClick(View v) {
                userField = (EditText)findViewById(R.id.user_field_id);
                String userID = userField.getText().toString();

                passwordField = (EditText)findViewById(R.id.password_field_id);
                String password = passwordField.getText().toString();

                if(validateUser(userID, password)){
                    Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                    startActivity(intent);
                }

                passwordField.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
//                final int DRAWABLE_LEFT = 0;
//                final int DRAWABLE_TOP = 1;
                        final int DRAWABLE_RIGHT = 2;
//                final int DRAWABLE_BOTTOM = 3;

                        if(event.getAction() == MotionEvent.ACTION_UP) {
                            if(event.getRawX() >= (passwordField.getRight() - passwordField.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                                // your action here
                                isVisible=!isVisible;
                                if (isVisible)
                                {
                                    // Show Password
                                    passwordField.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                                }
                                else
                                {
                                    // Hide Password
                                    passwordField.setTransformationMethod(PasswordTransformationMethod.getInstance());
                                }

                                return true;
                            }
                        }
                        return false;
                    }
                });



            }
        });
    }

    boolean validateUser(String userID, String password) {
        boolean isValidUser = false, isValidPassword = false;

        if (userID.isEmpty()) {
            userField.setError("UserID is required");
            isValidUser = false;

        }else if(!Patterns.EMAIL_ADDRESS.matcher(userID).matches()){
            userField.setError("Enter a valid email");
            isValidUser = false;

        }else{


            //hit server
            isValidUser = true;

        }

        if(password.isEmpty()){
            passwordField.setError("Password is required");
            isValidPassword = false;
        }else if(password.length() < 8){
            passwordField.setError("password must be 8 characters or more");
            isValidPassword = false;
        }else {

            //hit server
            isValidPassword = true;

        }

        return isValidUser && isValidPassword;

    }

}
