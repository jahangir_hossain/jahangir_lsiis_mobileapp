package com.example.edoc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

public class SignUpEmailVerificationActivity extends AppCompatActivity {

    EditText vefiryCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //remove the action bar & make the app full screen
        requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        getSupportActionBar().hide(); // hide the title bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN); //enable full screen

        setContentView(R.layout.activity_sign_up_email_verification);

        vefiryCode =(EditText)findViewById(R.id.code_field);
        vefiryCode.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() == 6) {

                    Intent intent = new Intent(getApplicationContext(), SignUpBasicInfoActivity.class);
                    startActivity(intent);

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }
}
